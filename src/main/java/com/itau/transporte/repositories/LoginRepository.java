package com.itau.transporte.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.transporte.models.Login;

public interface LoginRepository extends CrudRepository<Login, Login>{
	
}
