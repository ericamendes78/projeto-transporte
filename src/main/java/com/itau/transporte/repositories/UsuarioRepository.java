package com.itau.transporte.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.transporte.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

	public Optional<Usuario> findByUsername(String username);
}
