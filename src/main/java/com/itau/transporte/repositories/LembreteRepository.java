package com.itau.transporte.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.transporte.models.Lembrete;

public interface LembreteRepository extends CrudRepository< Lembrete, Long> {
	

}
