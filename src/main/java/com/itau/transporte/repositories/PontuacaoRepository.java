package com.itau.transporte.repositories;

import org.springframework.data.repository.CrudRepository;
import com.itau.transporte.models.Pontuacao;

public interface PontuacaoRepository extends CrudRepository<Pontuacao, Long>{

}
