package com.itau.transporte.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Endereco {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String logradouro;
	private String complemento;
	private String bairro;
	
	@NotNull
	private long CEP;
	
	@NotNull
	private int numero;
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void SetLogradouro(String endereco) {
		this.logradouro = endereco;
	}

	public String getCompleto() {
		return complemento;
	}

	public void setCompleto(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public long getCEP() {
		return CEP;
	}

	public void setCEP(long CEP) {
		this.CEP = CEP;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

}
