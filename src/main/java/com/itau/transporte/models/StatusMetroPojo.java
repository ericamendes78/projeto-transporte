package com.itau.transporte.models;

import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonAlias;

public class StatusMetroPojo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonAlias("ListLineStatus")
	private ArrayList<LinhaPojo> listLineStatusPojo;

	public ArrayList<LinhaPojo> getListLineStatusPojo() {
		return listLineStatusPojo;
	}

	public void setListLineStatusPojo(ArrayList<LinhaPojo> listLineStatusPojo) {
		this.listLineStatusPojo = listLineStatusPojo;
	}

}
