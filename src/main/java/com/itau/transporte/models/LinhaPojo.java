package com.itau.transporte.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

public class LinhaPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@JsonAlias("Id")
	private String id;
	@JsonAlias("Color")
	private String color;
	@JsonAlias("Line")
	private String line;
	@JsonAlias("LineRaw")
	private String lineRaw;
	@JsonAlias("StatusMetro")
	private String statusMetro;
	@JsonAlias("Status")
	private int status;
	@JsonAlias("Description")
	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getLineRaw() {
		return lineRaw;
	}

	public void setLineRaw(String lineRaw) {
		this.lineRaw = lineRaw;
	}

	public String getStatusMetro() {
		return statusMetro;
	}

	public void setStatusMetro(String statusMetro) {
		this.statusMetro = statusMetro;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
