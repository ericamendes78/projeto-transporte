package com.itau.transporte.models;

public class TotalVoiceResponse {
	
	private long id;
	private int motivo;
	private String mensagem;
	private boolean sucesso;
	private int status;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public int getMotivo() {
		return motivo;
	}
	
	public void setMotivo(int motivo) {
		this.motivo = motivo;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public boolean isSucesso() {
		return sucesso;
	}
	
	public void setSucesso(boolean sucesso) {
		this.sucesso = sucesso;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}

}
