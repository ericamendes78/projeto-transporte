package com.itau.transporte.models.enums;

public enum TipoLembrete {
	NENHUM(0,""),
	SMS(1,"SMS"),
	LIGACAO(2, "Ligação"),
	AMBOS(3,"Ambos");

	private int codigo;
	private String descricao;
	
	private TipoLembrete(int codigo, String descricao) {
		setCodigo(codigo);
		setDescricao(descricao);
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
