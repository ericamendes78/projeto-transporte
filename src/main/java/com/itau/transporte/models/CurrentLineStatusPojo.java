package com.itau.transporte.models;

import java.io.Serializable;

public class CurrentLineStatusPojo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dateUpdateFormated;
	private String status;
	private String icon;
	private String finalDescription;
	
	
	public String getDateUpdateFormated() {
		return dateUpdateFormated;
	}
	public void setDateUpdateFormated(String dateUpdateFormated) {
		this.dateUpdateFormated = dateUpdateFormated;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getFinalDescription() {
		return finalDescription;
	}
	public void setFinalDescription(String finalDescription) {
		this.finalDescription = finalDescription;
	}
	
	

}
