package com.itau.transporte.models.sptrans;

public class PrevisaoParada extends  Previsao{
	
	private ParadaLinha[] p;

	public ParadaLinha[] getP() {
		return p;
	}
	public void setP(ParadaLinha[] p) {
		this.p = p;
	}

}
