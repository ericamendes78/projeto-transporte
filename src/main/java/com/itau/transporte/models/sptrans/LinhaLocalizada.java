package com.itau.transporte.models.sptrans;

import java.util.ArrayList;

public class LinhaLocalizada {
	
	private String c;
	private int cl;
	private int sl;
	private String lt0;
	private String lt1;
	private int qv;
	private VeiculoLocalizado[] vs;
	
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public int getCl() {
		return cl;
	}
	public void setCl(int cl) {
		this.cl = cl;
	}
	public int getSl() {
		return sl;
	}
	public void setSl(int sl) {
		this.sl = sl;
	}
	public String getLt0() {
		return lt0;
	}
	public void setLt0(String lt0) {
		this.lt0 = lt0;
	}
	public String getLt1() {
		return lt1;
	}
	public void setLt1(String lt1) {
		this.lt1 = lt1;
	}
	public int getQv() {
		return qv;
	}
	public void setQv(int qv) {
		this.qv = qv;
	}
	public VeiculoLocalizado[] getVs() {
		return vs;
	}
	public void setVs(VeiculoLocalizado[] vs) {
		this.vs = vs;
	}
	
}
