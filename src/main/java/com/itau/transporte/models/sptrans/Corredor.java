package com.itau.transporte.models.sptrans;

public class Corredor {
	
	private String cc;
	private String cn;
	
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	
}
