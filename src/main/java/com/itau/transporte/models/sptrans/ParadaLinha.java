package com.itau.transporte.models.sptrans;

public class ParadaLinha extends Parada {
	
	private LinhaLocalizada[] l;
	
	public LinhaLocalizada[] getL() {
		return l;
	}
	public void setL(LinhaLocalizada[] l) {
		this.l = l;
	}

}
