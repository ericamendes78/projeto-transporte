package com.itau.transporte.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Telefone {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private int ddi;
	
	@NotNull
	private int ddd;
	
	@NotNull
	private long numero;
	
	private String observacao;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public int getDDI() {
		return ddi;
	}
	
	public void setDDI(int ddi) {
		this.ddi = ddi;
	}
	
	public int getDDD() {
		return ddd;
	}
	
	public void setDDD(int ddd) {
		this.ddd = ddd;
	}
	
	public long getNumero() {
		return numero;
	}
	
	public void setNumero(long numero) {
		this.numero = numero;
	}
	
	public String getObservacao() {
		return observacao;
	}
	
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	public String getTelefone() {
		return "+" + getDDI() + getDDD() + getNumero();
	}

}
