package com.itau.transporte.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Pontuacao {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private int quantidadeViagens;
	
	@NotNull
	private double pontuacao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getQuantidadeViagens() {
		return quantidadeViagens;
	}

	public void setQuantidadeViagens(int quantidadeViagens) {
		this.quantidadeViagens += quantidadeViagens;
	}

	public double getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(double pontuacao) {
		this.pontuacao += pontuacao;
	}
	
    	
}
