package com.itau.transporte.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.itau.transporte.models.enums.TipoLembrete;

@Entity
public class Lembrete {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String dataHora;	
	private String descricao;
	private TipoLembrete tipo;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getDataHora() {
		return dataHora;
	}
	
	public void setDataHora(String dataHora) {
		this.dataHora = dataHora;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public TipoLembrete getTipo() {
		return tipo;
	}
	
	public void setTipo(TipoLembrete tipo) {
		this.tipo = tipo;
	}

}
