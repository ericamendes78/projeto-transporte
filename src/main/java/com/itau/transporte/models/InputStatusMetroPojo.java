package com.itau.transporte.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

public class InputStatusMetroPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonAlias("CurrentLineStatus")
	private CurrentLineStatusPojo currentLineStatusPojo;
	@JsonAlias("StatusMetro")
	private StatusMetroPojo statusMetroPojo;
	@JsonAlias("DateUpdateMetro")
	private String dateUpdateMetro;
	
	
	public CurrentLineStatusPojo getCurrentLineStatusPojo() {
		return currentLineStatusPojo;
	}
	public void setCurrentLineStatusPojo(CurrentLineStatusPojo currentLineStatusPojo) {
		this.currentLineStatusPojo = currentLineStatusPojo;
	}
	public StatusMetroPojo getStatusMetroPojo() {
		return statusMetroPojo;
	}
	public void setStatusMetroPojo(StatusMetroPojo statusMetroPojo) {
		this.statusMetroPojo = statusMetroPojo;
	}
	public String getDateUpdateMetro() {
		return dateUpdateMetro;
	}
	public void setDateUpdateMetro(String dateUpdateMetro) {
		this.dateUpdateMetro = dateUpdateMetro;
	}
	
	
}
