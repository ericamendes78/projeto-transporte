package com.itau.transporte.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.transporte.models.InputStatusMetroPojo;

@Service
public class StatusMetroService {

	private RestTemplate restTemplate = new RestTemplate();

	public InputStatusMetroPojo getStatusLinhas() {
		String url = "http://www.viaquatro.com.br/generic/Main/LineStatus";

		return restTemplate.getForObject(url, InputStatusMetroPojo.class);

	}
	

}
