package com.itau.transporte.service;

import br.com.totalvoice.TotalVoiceClient;
import br.com.totalvoice.api.Sms;
import br.com.totalvoice.api.Tts;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.itau.transporte.models.Telefone;

@Service
public class TotalVoiceService {
	
	private TotalVoiceClient client;

	public TotalVoiceService() {
		client = new TotalVoiceClient("f37cd163424dedee64fedf2084d3023c");
	}

	public void enviarSMS(Telefone telefone) {

		try {
			 Sms sms = new Sms(client);

			 JSONObject result = sms.enviar(telefone.getTelefone(), telefone.getObservacao());
			System.out.println(result);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}
	
	public void ligar(Telefone telefone) {

		try {
			Tts tts = new Tts(client);
			JSONObject result = tts.enviar(telefone.getTelefone(), telefone.getObservacao());
			System.out.println(result);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

}