package com.itau.transporte.service;

import java.util.stream.Collectors;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itau.transporte.models.sptrans.Corredor;
import com.itau.transporte.models.sptrans.Linha;
import com.itau.transporte.models.sptrans.Parada;
import com.itau.transporte.models.sptrans.Posicao;
import com.itau.transporte.models.sptrans.PosicaoLinha;
import com.itau.transporte.models.sptrans.PosicaoVeiculo;
import com.itau.transporte.models.sptrans.Previsao;
import com.itau.transporte.models.sptrans.PrevisaoLinha;
import com.itau.transporte.models.sptrans.PrevisaoParada;


@Service
public class SPTransService {
		
	private final String TOKEN = "8bc2b91a8786de31aa0cff42975856afd071cc63e46722b19736cb524e46d964";
	private final String VERSAO = "2.1";
	private final String URL = String.format("http://api.olhovivo.sptrans.com.br/v%s", VERSAO);

	private RestTemplate restTemplate = new RestTemplate();

	public org.springframework.http.HttpHeaders headers;

	public Boolean autenticar() {

		String url = URL + String.format("/Login/Autenticar?token=%s",TOKEN);
		ResponseEntity<Boolean> response = restTemplate.postForEntity(url,HttpMethod.POST, Boolean.class);

		headers = new  org.springframework.http.HttpHeaders();
		headers.set("Cookie",response.getHeaders().get("Set-Cookie").stream().collect(Collectors.joining(";")));

		return (Boolean) response.getBody();
	}
	
	public Linha[] buscarLinha(String termosBusca) {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + String.format("/Linha/Buscar?termosBusca=%s", termosBusca);
		ResponseEntity<Linha[]> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), Linha[].class);

		return response.getBody();
	}
	
	public Linha[] buscarLinhaSentido(String termosBusca, int sentido) {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + String.format("/Linha/BuscarLinhaSentido?termosBusca=%s&sentido=%o", termosBusca,sentido);
		ResponseEntity<Linha[]> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), Linha[].class);
	
		return response.getBody();
	}
	
	public Parada[] buscarParada(String termosBusca) {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + String.format("/Parada/Buscar?termosBusca=%s", termosBusca);
		ResponseEntity<Parada[]> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), Parada[].class);

		return response.getBody();
	}
	
	public Parada[] buscarParadaPorLinha(String codigoLinha) {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + String.format("/Parada/BuscarParadasPorLinha?codigoLinha=%s", codigoLinha);
		ResponseEntity<Parada[]> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), Parada[].class);

		return response.getBody();
	}
	
	public Parada[] buscarParadaPorCorredor(String codigoCorredor) {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + String.format("/Parada/BuscarParadasPorCorredor?codigoCorredor=%s", codigoCorredor);
		ResponseEntity<Parada[]> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), Parada[].class);

		return response.getBody();
	}
	
	public Corredor[] buscarCorredor() {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + "/Corredor";
		ResponseEntity<Corredor[]> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), Corredor[].class);

		return response.getBody();
	}
	
	public Posicao buscarPosicao() {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + "/Posicao" ;
		ResponseEntity<PosicaoLinha> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), PosicaoLinha.class);

		return response.getBody();
	}
	
	public Posicao buscarPosicaoLinha(String codigoLinha) {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + String.format("/Posicao?codigoLinha=%s", codigoLinha);
		ResponseEntity<PosicaoVeiculo> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), PosicaoVeiculo.class);

		return response.getBody();
	}
	
	public Previsao buscarPrevisao(String codigoParada ,String codigoLinha) {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + String.format("/Previsao?codigoParada=%s&codigoLinha=%s",codigoParada, codigoLinha);
		ResponseEntity<PrevisaoParada> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), PrevisaoParada.class);

		return response.getBody();
	}
	
	public Previsao buscarPrevisaoLinha(String codigoLinha) {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + String.format("/Previsao/Linha?codigoLinha=%s",codigoLinha);
		ResponseEntity<PrevisaoLinha> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), PrevisaoLinha.class);

		return response.getBody();
	}
	
	public Previsao buscarPrevisaoParada(String codigoParada) {

		if(!autenticar()) {
			System.out.print("Nao esta acessando API SPTrans.");
		}
			
		String url = URL + String.format("/Previsao/Parada?codigoParada=%s",codigoParada);
		ResponseEntity<PrevisaoParada> response = restTemplate.exchange(url, HttpMethod.GET,new HttpEntity<String>(headers), PrevisaoParada.class);

		return response.getBody();
	}

//	POST /Login/Autenticar?token={token} - ok
//	 GET /Linha/Buscar?termosBusca={termosBusca} - ok
//	 GET /Linha/CarregarDetalhes?codigoLinha={codigoLinha} - nok
//	 GET /Parada/Buscar?termosBusca={termosBusca} - ok
//	 GET /Parada/BuscarParadasPorLinha?codigoLinha={codigoLinha} - ok
//	 GET /Parada/BuscarParadasPorCorredor?codigoCorredor={codigoCorredor} - ok
//	 GET /Corredor - ok
//	 GET /Posicao - ok
//	 GET /Posicao?codigoLinha={codigoLinha} - ok
//	 GET /Previsao?codigoParada={codigoParada}&codigoLinha={codigoLinha} - ok
//	 GET /Previsao/Linha?codigoLinha={codigoLinha} - ok
//	 GET /Previsao/Parada?codigoParada={codigoParada} - ok
	
}