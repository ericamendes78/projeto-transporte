package com.itau.transporte.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.transporte.models.Lembrete;
import com.itau.transporte.repositories.LembreteRepository;

@RestController
@CrossOrigin
public class LembreteController {

	@Autowired
	LembreteRepository lembreteRepository;

	@RequestMapping(method=RequestMethod.POST, path="/lembrete")
	public Lembrete criarLembrete(@Valid @RequestBody Lembrete lembrete) {
		return lembreteRepository.save(lembrete);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/lembretes")
	public Iterable<Lembrete> buscarLembretes() {
		return lembreteRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/lembrete/{id}")
	public ResponseEntity<?> buscarLembretes(@PathVariable long id) {
		Optional<Lembrete> optionalLembrete = lembreteRepository.findById(id);
		
		if(!optionalLembrete.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(optionalLembrete.get());
	}
	
	@RequestMapping(method=RequestMethod.PUT, path="/lembrete/{id}")
	public ResponseEntity<?> alterarLembrete(@PathVariable long id, @Valid @RequestBody Lembrete lembrete) {
		Optional<Lembrete> optionalLembrete = lembreteRepository.findById(id);
		
		if(!optionalLembrete.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Lembrete lembreteDoBanco = optionalLembrete.get();
		
		lembreteDoBanco.setTipo(lembrete.getTipo());
		lembreteDoBanco.setDescricao(lembrete.getDescricao());
		lembreteDoBanco.setDataHora(lembrete.getDataHora());

		Lembrete lembreteSalvo = lembreteRepository.save(lembreteDoBanco);
		
		return ResponseEntity.ok().body(lembreteSalvo);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/lembrete/{id}")
	public ResponseEntity<?> deletarLembrete(@PathVariable long id) {
		Optional<Lembrete> optionalLembrete = lembreteRepository.findById(id);
		
		if(!optionalLembrete.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		lembreteRepository.deleteById(id);
		
		return ResponseEntity.ok().build();
	}
}
