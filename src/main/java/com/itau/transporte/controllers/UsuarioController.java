package com.itau.transporte.controllers;

import java.util.ArrayList;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.transporte.models.Usuario;
import com.itau.transporte.inputs.InputCadastroUsuario;
import com.itau.transporte.models.Endereco;
import com.itau.transporte.models.Lembrete;
import com.itau.transporte.models.Login;
import com.itau.transporte.models.Pontuacao;
import com.itau.transporte.models.Telefone;
import com.itau.transporte.repositories.UsuarioRepository;
import com.itau.transporte.repositories.LoginRepository;
import com.itau.transporte.service.PasswordService;

@RestController
@CrossOrigin
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	LoginRepository loginRepository;

	@Autowired
	PasswordService passwordService;

	@RequestMapping(method = RequestMethod.POST, path = "/usuario")
	public Usuario criarUsuario(@Valid @RequestBody InputCadastroUsuario inputCadastroUsuario) {

		Usuario usuario = new Usuario();
		usuario.setCpf(inputCadastroUsuario.getCpf());
		usuario.setDataDeNascimento(inputCadastroUsuario.getDataDeNascimento());
		usuario.setEmail(inputCadastroUsuario.getEmail());

		Endereco endereco = new Endereco();
		endereco.setBairro(inputCadastroUsuario.getBairro());
		endereco.setCEP(inputCadastroUsuario.getCep());
		endereco.setCompleto(inputCadastroUsuario.getComplemento());
		endereco.SetLogradouro(inputCadastroUsuario.getLogradouro());
		endereco.setNumero(inputCadastroUsuario.getNumeroLogradouro());

		ArrayList<Endereco> enderecos = new ArrayList<>();

		enderecos.add(endereco);
		usuario.setEnderecos(enderecos);

		Lembrete lembrete = new Lembrete();
		lembrete.setTipo(inputCadastroUsuario.getTipoLembrete());

		ArrayList<Lembrete> lembretes = new ArrayList<>();
		lembretes.add(lembrete);
		usuario.setLembretes(lembretes);

		usuario.setNome(inputCadastroUsuario.getNome());
		usuario.setSenha(inputCadastroUsuario.getSenha());

		Pontuacao pontuacao = new Pontuacao();

		pontuacao.setPontuacao(10);
		usuario.setPontuacao(pontuacao);

		ArrayList<Telefone> telefones = new ArrayList<>();

		Telefone telefone = new Telefone();
		telefone.setDDD(inputCadastroUsuario.getDdd());
		telefone.setDDI(inputCadastroUsuario.getDdi());
		telefone.setNumero(inputCadastroUsuario.getNumeroTelefone());

		telefones.add(telefone);
		usuario.setTelefones(telefones);

		usuario.setUsername(inputCadastroUsuario.getUsername());

		String hash = passwordService.gerarHash(usuario.getSenha());
		usuario.setSenha(hash);

		Login login = new Login();

		login.setUsername(usuario.getNome());
		login.setSenha(usuario.getSenha());

		loginRepository.save(login);

		return usuarioRepository.save(usuario);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/usuarios")
	public Iterable<Usuario> buscarUsuarios() {
		return usuarioRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.GET, path = "/usuario/{id}")
	public ResponseEntity<?> buscarUsuarios(@PathVariable long id) {
		Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);

		if (!optionalUsuario.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok().body(optionalUsuario.get());
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/usuario/username/{username}")
	public ResponseEntity<?> buscarUsuarioNome(@PathVariable String username) {
		Optional<Usuario> optionalUsuario = usuarioRepository.findByUsername(username);

		if (!optionalUsuario.isPresent()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok().body(optionalUsuario.get());
	}

	/*
	 * @RequestMapping(method = RequestMethod.PUT, path = "/usuario/{id}") public
	 * ResponseEntity<?> alterarUsuario(@PathVariable long id, @Valid @RequestBody
	 * Usuario usuario) { Optional<Usuario> optionalUsuario =
	 * usuarioRepository.findById(id);
	 * 
	 * if (!optionalUsuario.isPresent()) { return ResponseEntity.notFound().build();
	 * }
	 * 
	 * Usuario usuarioDoBanco = optionalUsuario.get();
	 * 
	 * usuarioDoBanco.setNome(usuario.getNome());
	 * usuarioDoBanco.setEmail(usuario.getEmail());
	 * usuarioDoBanco.setTelefones(usuario.getTelefones());
	 * usuarioDoBanco.setLembretes(usuario.getLembretes());
	 * usuarioDoBanco.setEnderecos(usuario.getEnderecos());
	 * 
	 * Usuario usuarioSalvo = usuarioRepository.save(usuarioDoBanco);
	 * 
	 * return ResponseEntity.ok().body(usuarioSalvo); }
	 * 
	 * @RequestMapping(method = RequestMethod.DELETE, path = "/usuario/{id}") public
	 * ResponseEntity<?> deletarUsuario(@PathVariable long id) { Optional<Usuario>
	 * optionalUsuario = usuarioRepository.findById(id);
	 * 
	 * if (!optionalUsuario.isPresent()) { return ResponseEntity.notFound().build();
	 * }
	 * 
	 * usuarioRepository.deleteById(id);
	 * 
	 * return ResponseEntity.ok().build(); }
	 */

}
