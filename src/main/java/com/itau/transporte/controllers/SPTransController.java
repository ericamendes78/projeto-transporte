package com.itau.transporte.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.transporte.models.sptrans.Corredor;
import com.itau.transporte.models.sptrans.Linha;
import com.itau.transporte.models.sptrans.Parada;
import com.itau.transporte.models.sptrans.Posicao;
import com.itau.transporte.models.sptrans.Previsao;
import com.itau.transporte.service.SPTransService;

@RestController
@CrossOrigin
public class SPTransController {
	
	@Autowired
	SPTransService sptransService;

	@RequestMapping(method = RequestMethod.GET, path = "/onibus/linha/{descricao}")
	public Linha[] getLinha(@PathVariable String descricao) {
		return sptransService.buscarLinha(descricao);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/linha/{descricao}/{sentido}")
	public Linha[] getLinha(@PathVariable String descricao, @PathVariable int sentido) {
		return sptransService.buscarLinhaSentido(descricao,sentido);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/parada/{descricao}")
	public Parada[] getParada(@PathVariable String descricao) {
		return sptransService.buscarParada(descricao);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/parada/linha/{codigoLinha}")
	public Parada[] getParadaLinha(@PathVariable String codigoLinha) {
		return sptransService.buscarParadaPorLinha(codigoLinha);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/parada/corredor/{codigoCorredor}")
	public Parada[] getParadaCorredor(@PathVariable String codigoCorredor) {
		return sptransService.buscarParadaPorCorredor(codigoCorredor);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/corredor")
	public Corredor[] getCorredor() {
		return sptransService.buscarCorredor();
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/posicao")
	public Posicao getPosicao() {
		return sptransService.buscarPosicao();
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/posicao/{codigoLinha}")
	public Posicao getPosicaoLinha(@PathVariable String codigoLinha) {
		return sptransService.buscarPosicaoLinha(codigoLinha);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/previsao/{codigoParada}/{codigoLinha}")
	public Previsao getPrevisao(@PathVariable String codigoParada,@PathVariable String codigoLinha) {
		return sptransService.buscarPrevisao(codigoParada,codigoLinha);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/previsao/linha/{codigoLinha}")
	public Previsao getPrevisaoLinha(@PathVariable String codigoLinha) {
		return sptransService.buscarPrevisaoLinha(codigoLinha);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/onibus/previsao/parada/{codigoParada}")
	public Previsao getPrevisaoParada(@PathVariable String codigoParada) {
		return sptransService.buscarPrevisaoParada(codigoParada);
	}


}
