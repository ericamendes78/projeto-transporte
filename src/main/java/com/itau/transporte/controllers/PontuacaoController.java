package com.itau.transporte.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.itau.transporte.models.Pontuacao;
import com.itau.transporte.repositories.PontuacaoRepository;

@RestController
@CrossOrigin
public class PontuacaoController {
	
	@Autowired
	PontuacaoRepository pontuacaoRepository;
		
	@RequestMapping(method=RequestMethod.POST, path="/pontuacao")
	public Pontuacao criarPontuacao() {
		
		Pontuacao pontuacao = new Pontuacao();
		
		pontuacao.setPontuacao(0);
		pontuacao.setQuantidadeViagens(0);
		
		return pontuacaoRepository.save(pontuacao);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/pontuacao/{id}")
	public ResponseEntity<?> buscarPontuacao(@PathVariable long id) {
		Optional<Pontuacao> optionalPontuacao = pontuacaoRepository.findById(id);
		
		if(!optionalPontuacao.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok().body(optionalPontuacao.get());
	}
	
	@RequestMapping(method=RequestMethod.PUT, path="/pontuacao/{id}")
	public ResponseEntity<?> alterarPontuacao(@PathVariable long id) {
		Optional<Pontuacao> optionalPontuacao = pontuacaoRepository.findById(id);
		
		if(!optionalPontuacao.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		Pontuacao pontuacaoDoBanco = optionalPontuacao.get();
		
		pontuacaoDoBanco.setQuantidadeViagens(1);
		
		if((pontuacaoDoBanco.getQuantidadeViagens() % 10) == 0) {
			pontuacaoDoBanco.setPontuacao(11);
		}else {
			pontuacaoDoBanco.setPontuacao(1);
		}
		
		Pontuacao pontuacaoSalvo = pontuacaoRepository.save(pontuacaoDoBanco);
		
		return ResponseEntity.ok().body(pontuacaoSalvo);
	}

}
