package com.itau.transporte.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.transporte.models.Telefone;
import com.itau.transporte.service.TotalVoiceService;

@RestController
@CrossOrigin
public class TotalVoiceController {
	
	@Autowired
	TotalVoiceService totalVoiceService;
	
	@RequestMapping(method=RequestMethod.POST, path="/lembrete/disparar/sms")
	public void enviarSMS(@Valid @RequestBody Telefone telefone) {
		totalVoiceService.enviarSMS(telefone);
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/lembrete/disparar/ligacao")
	public void ligar(@Valid @RequestBody Telefone telefone) {
		totalVoiceService.ligar(telefone);
	}

}
