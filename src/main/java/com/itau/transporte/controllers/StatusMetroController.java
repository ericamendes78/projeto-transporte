package com.itau.transporte.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.transporte.models.InputStatusMetroPojo;
import com.itau.transporte.service.StatusMetroService;

@RestController
@CrossOrigin
public class StatusMetroController {

	@Autowired
	StatusMetroService statusMetroService;

	@RequestMapping(method = RequestMethod.GET, path = "status/metro")
	public InputStatusMetroPojo getStausLinhasMetro() {

		return statusMetroService.getStatusLinhas();

	}
}
