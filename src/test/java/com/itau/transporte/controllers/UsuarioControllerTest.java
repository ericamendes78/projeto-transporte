package com.itau.transporte.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.transporte.inputs.InputCadastroUsuario;
import com.itau.transporte.models.Endereco;
import com.itau.transporte.models.Lembrete;
import com.itau.transporte.models.Login;
import com.itau.transporte.models.Pontuacao;
import com.itau.transporte.models.Telefone;
import com.itau.transporte.models.Usuario;
import com.itau.transporte.repositories.LoginRepository;
import com.itau.transporte.repositories.UsuarioRepository;
import com.itau.transporte.service.PasswordService;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.http.MediaType;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

	// MockBean em todos os repositorios
	@MockBean
	UsuarioRepository usuarioRepository;

	@MockBean
	LoginRepository loginRepository;

	@Autowired
	UsuarioController usuarioController;

	@MockBean
	PasswordService passwordService;

	// nao tenho certeza
	ObjectMapper mapper = new ObjectMapper();
	@Autowired
	private MockMvc mockMvc;
	/////////////////////

	@Test
	public void criarUsuarioTest() throws Exception {

		InputCadastroUsuario inputCadastroUsuario = new InputCadastroUsuario();
		inputCadastroUsuario.setBairro("aaaaa");
		inputCadastroUsuario.setCep(11111);
		inputCadastroUsuario.setComplemento("aaaaa");
		inputCadastroUsuario.setCpf("1231321321");
		inputCadastroUsuario.setDataDeNascimento("213213");
		inputCadastroUsuario.setDdd(55);
		inputCadastroUsuario.setDdi(11);
		inputCadastroUsuario.setEmail("jwijwijwijwi");
		inputCadastroUsuario.setLogradouro("iwjiejieijeie");
		inputCadastroUsuario.setNome("euheuheuhe");
		inputCadastroUsuario.setNumeroLogradouro(1212);
		inputCadastroUsuario.setNumeroTelefone(12132321);
		inputCadastroUsuario.setSenha("senha1234");
		inputCadastroUsuario.setUsername("josejose");

		Usuario usuario = new Usuario();
		usuario.setCpf(inputCadastroUsuario.getCpf());
		usuario.setDataDeNascimento(inputCadastroUsuario.getDataDeNascimento());
		usuario.setEmail(inputCadastroUsuario.getEmail());

		Endereco endereco = new Endereco();
		endereco.setBairro(inputCadastroUsuario.getBairro());
		endereco.setCEP(inputCadastroUsuario.getCep());
		endereco.setCompleto(inputCadastroUsuario.getComplemento());
		endereco.SetLogradouro(inputCadastroUsuario.getLogradouro());
		endereco.setNumero(inputCadastroUsuario.getNumeroLogradouro());

		ArrayList<Endereco> enderecos = new ArrayList<>();

		enderecos.add(endereco);
		usuario.setEnderecos(enderecos);

		Lembrete lembrete = new Lembrete();
		lembrete.setTipo(inputCadastroUsuario.getTipoLembrete());

		ArrayList<Lembrete> lembretes = new ArrayList<>();
		lembretes.add(lembrete);
		usuario.setLembretes(lembretes);

		usuario.setNome(inputCadastroUsuario.getNome());
		usuario.setSenha(inputCadastroUsuario.getSenha());

		Pontuacao pontuacao = new Pontuacao();

		pontuacao.setPontuacao(10);
		usuario.setPontuacao(pontuacao);

		ArrayList<Telefone> telefones = new ArrayList<>();

		Telefone telefone = new Telefone();
		telefone.setDDD(inputCadastroUsuario.getDdd());
		telefone.setDDI(inputCadastroUsuario.getDdi());
		telefone.setNumero(inputCadastroUsuario.getNumeroTelefone());

		telefones.add(telefone);
		usuario.setTelefones(telefones);

		usuario.setUsername(inputCadastroUsuario.getUsername());

		String hash = passwordService.gerarHash(usuario.getSenha());
		usuario.setSenha(hash);

		Login login = new Login();

		login.setUsername(usuario.getNome());
		login.setSenha(usuario.getSenha());

		when(usuarioRepository.save(any(Usuario.class))).thenReturn(usuario);

		String json = mapper.writeValueAsString(usuario);
		mockMvc.perform(post("/usuario").content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.username", equalTo("josejose")));

	}

	@Test
	public void buscarUsuariosTest() throws Exception {

		InputCadastroUsuario inputCadastroUsuario = new InputCadastroUsuario();
		inputCadastroUsuario.setBairro("aaaaa");
		inputCadastroUsuario.setCep(11111);
		inputCadastroUsuario.setComplemento("aaaaa");
		inputCadastroUsuario.setCpf("1231321321");
		inputCadastroUsuario.setDataDeNascimento("213213");
		inputCadastroUsuario.setDdd(55);
		inputCadastroUsuario.setDdi(11);
		inputCadastroUsuario.setEmail("jwijwijwijwi");
		inputCadastroUsuario.setLogradouro("iwjiejieijeie");
		inputCadastroUsuario.setNome("euheuheuhe");
		inputCadastroUsuario.setNumeroLogradouro(1212);
		inputCadastroUsuario.setNumeroTelefone(12132321);
		inputCadastroUsuario.setSenha("senha1234");
		inputCadastroUsuario.setUsername("josejose");

		Usuario usuario = new Usuario();
		usuario.setCpf(inputCadastroUsuario.getCpf());
		usuario.setDataDeNascimento(inputCadastroUsuario.getDataDeNascimento());
		usuario.setEmail(inputCadastroUsuario.getEmail());

		Endereco endereco = new Endereco();
		endereco.setBairro(inputCadastroUsuario.getBairro());
		endereco.setCEP(inputCadastroUsuario.getCep());
		endereco.setCompleto(inputCadastroUsuario.getComplemento());
		endereco.SetLogradouro(inputCadastroUsuario.getLogradouro());
		endereco.setNumero(inputCadastroUsuario.getNumeroLogradouro());

		ArrayList<Endereco> enderecos = new ArrayList<>();

		enderecos.add(endereco);
		usuario.setEnderecos(enderecos);

		Lembrete lembrete = new Lembrete();
		lembrete.setTipo(inputCadastroUsuario.getTipoLembrete());

		ArrayList<Lembrete> lembretes = new ArrayList<>();
		lembretes.add(lembrete);
		usuario.setLembretes(lembretes);

		usuario.setNome(inputCadastroUsuario.getNome());
		usuario.setSenha(inputCadastroUsuario.getSenha());

		Pontuacao pontuacao = new Pontuacao();

		pontuacao.setPontuacao(10);
		usuario.setPontuacao(pontuacao);

		ArrayList<Telefone> telefones = new ArrayList<>();

		Telefone telefone = new Telefone();
		telefone.setDDD(inputCadastroUsuario.getDdd());
		telefone.setDDI(inputCadastroUsuario.getDdi());
		telefone.setNumero(inputCadastroUsuario.getNumeroTelefone());

		telefones.add(telefone);
		usuario.setTelefones(telefones);

		usuario.setUsername(inputCadastroUsuario.getUsername());

		String hash = passwordService.gerarHash(usuario.getSenha());
		usuario.setSenha(hash);

		Login login = new Login();

		login.setUsername(usuario.getNome());
		login.setSenha(usuario.getSenha());

		when(usuarioRepository.findAll()).thenReturn(Arrays.asList(usuario));

		String json = mapper.writeValueAsString(usuario);

		mockMvc.perform(get("/usuarios").content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].username", equalTo("josejose")));
	}

	@Test
	public void buscarUsuarioTest() throws Exception {

		Usuario usuario = new Usuario();

		usuario.setId(1L);
		usuario.setUsername("josejose");

		Optional<Usuario> optionalUsuario = Optional.of(usuario);

		when(usuarioRepository.findById(1L)).thenReturn(optionalUsuario);


		mockMvc.perform(get("/usuario/1").contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.username", equalTo("josejose")));
		
	
	}
	
		
	
}
